package extentreportconfig;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
 
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
 
public class ExtentReport
{
    public static ExtentHtmlReporter htmlReporter;
    public static ExtentReports extent;
    public static ExtentTest test;
    public static WebDriver driver;
    
    
    @BeforeSuite
    public void setUp()
    {
        htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +"/test-output/SmokeRuns.html");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
         
       // extent.setSystemInfo("OS", "Mac Sierra");
       // extent.setSystemInfo("Host Name", "Krishna");
        extent.setSystemInfo("Environment", "testlocal");
        extent.setSystemInfo("User Name", "Automation User");
         
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setDocumentTitle("AutomationTesting");
        htmlReporter.config().setReportName("Automation Report");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTheme(Theme.DARK);
    }
          
    
    @AfterMethod
    public void getResult(ITestResult result) throws IOException
    {
        if (result.getStatus() == ITestResult.FAILURE)
        {
        	//  driver = new ChromeDriver();
            String screenShotPath = TakeShot.capture(driver, "screenShotName");
         //   test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" test case FAILED due to issues below:", ExtentColor.RED));
            test.fail(result.getThrowable());
            test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));

        }
        else if(result.getStatus() == ITestResult.SUCCESS)
        {
         //   test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
        }
        else
        {
         //   test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
            test.skip(result.getThrowable());
        }
        extent.flush();

    }
    
    @AfterSuite
    public void tearDown()
    {
        extent.flush();
    }
}