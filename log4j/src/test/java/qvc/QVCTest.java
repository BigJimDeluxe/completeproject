package qvc;


import org.testng.annotations.Test;
import org.testng.Assert;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Action;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import extentreportconfig.ExtentReport;
import extentreportconfig.TakeShot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class QVCTest extends ExtentReport{
	
	  private static final Logger LOGGER = LogManager.getLogger(QVCTest.class.getName());
	
	public WebDriver driver; 
	String appURL = "http://qvc.com";

	@Test
	@BeforeMethod
	//@Parameters("appURL")
	public void testSetUp() {
		
		
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\Admin\\chromedriver.exe");
	
		  driver = new ChromeDriver();	
			driver.navigate().to(appURL);
		 // driver.get(appURL);
			driver.manage().window().maximize();}
		
	

	@Test
	public void verifyQVCPageTitle1() throws InterruptedException {
		
		String getTitle = driver.getTitle();
		Assert.assertEquals(getTitle, "QVC | Shop QVC� For Today�s Special Value & Top Brands At The Official Site");

		//driver.findElement(By.xpath("//a[#'shopByCategory']"));
		
		WebElement shop = driver.findElement(By.xpath("//div[@class='menuLinkCell shopByCat']"));
		
		WebElement acct = driver.findElement(By.xpath("//div[@class='acctGreeting acctUnAuth']"));
		WebElement signin = driver.findElement(By.xpath("//a[@title='Sign In']"));
		

			Actions action = new Actions(driver);
			
			action.moveToElement(shop).perform();
			action.moveToElement(acct).perform();
	
	//	action.moveToElement(acct).click(signin).build().perform();
			
			driver.findElement(By.xpath("//span[.='Cart']")).click();
			Thread.sleep(1000);
			String carttext = driver.findElement(By.xpath("//*[@class='h2 title' and contains(text(), 'aved')]")).getText();
	
			
			// //div[@class='bubble-title' and contains(text(), 'Cover')]
				
				
			//String bodyText = driver.findElement(By.tagName("body")).getText();
			//Assert.assertEquals("Text not found!", carttext);
			Assert.assertEquals(carttext, "Saved Items");
			//driver.findElement(By.xpath("//*[@id='pageContent' and contains(text(),'empty')]")).getText();
		//	Assert.
			//Assert.assertEquals(getTitle, "test");


	  {
	        LOGGER.debug("Debug Message Logged !!!");
	        LOGGER.info("Info Message Logged !!!");
	       // LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
	    }
	    
	}
	
	    {
	        LOGGER.debug("Debug Message Logged !!!");
	        LOGGER.info("Info Message Logged !!!");
	    }
	    
	
@AfterMethod(alwaysRun=true)
@Parameters({"author", "group"})
public void getResult(ITestResult result, String author, String group) throws IOException
{
   // {
        LOGGER.debug("Debug Message Logged !!!");
        LOGGER.info("Info Message Logged !!!");
       // LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
  //  }
	String methodName = result.getMethod().getMethodName();

	String methodCut = methodName.substring(10);

    if (result.getStatus() == ITestResult.FAILURE)
    {
    	test = extent.createTest(methodName);
    	test.assignCategory("Amazon. " +group);
    	test.assignAuthor(author);
    	test.info("verify amazon "  +methodCut+  " process works correctly");
    	test.info("UserID: AutomationUser1");
        String screenShotPath = TakeShot.capture(driver, "screenShotName");
        test.log(Status.FAIL, MarkupHelper.createLabel(methodName+" test case FAILED due to below issues:", ExtentColor.RED));
        test.fail(result.getThrowable());
        Assert.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
    }
   else if(result.getStatus() == ITestResult.SUCCESS)
    {
	   test = extent.createTest(methodName);
	test.assignCategory("Amazon. " +group);
	test.assignAuthor(author);
	test.info("verify amazon "  +methodCut+  " process works correctly");
	test.info("UserID: AutomationUser1");
        test.log(Status.PASS, MarkupHelper.createLabel(methodName+" test case PASSED", ExtentColor.GREEN));
    }
    else
    {
    	test = extent.createTest(methodName);
    	test.assignCategory("Amazon. " +group);
    	test.assignAuthor(author);
    	test.info("verify amazon "  +methodCut+  " process works correctly");
    	test.info("UserID: AutomationUser1");
        test.log(Status.SKIP, MarkupHelper.createLabel(methodName+" test case SKIPPED", ExtentColor.ORANGE));
        test.skip(result.getThrowable());
        
        LOGGER.debug("Debug Message Logged !!!");
        LOGGER.info("Info Message Logged !!!");
        LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
        
    }
    
    extent.flush();    


    driver.quit();
    
}

}