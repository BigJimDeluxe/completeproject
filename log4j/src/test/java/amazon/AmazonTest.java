package amazon;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import extentreportconfig.ExtentReport;
import extentreportconfig.TakeShot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AmazonTest extends ExtentReport{
	
	  private static final Logger LOGGER = LogManager.getLogger(AmazonTest.class.getName());
	
	public WebDriver driver; 
	//String appURL = "http://amazon.com";

	@BeforeMethod
	@Parameters("appURL")
	public void testSetUp(String appURL) {
		
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\Admin\\chromedriver.exe");
	
		  driver = new ChromeDriver();	
			//driver.navigate().to(appURL);
		  driver.get(appURL);
			driver.manage().window().maximize();}

	@Test
	public void verifyAmazonPageTitle1() throws InterruptedException {
		
		String getTitle = driver.getTitle();
		Assert.assertEquals(getTitle, "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & moree");
	    
	    {
	        LOGGER.debug("Debug Message Logged !!!");
	        LOGGER.info("Info Message Logged !!!");
	       // LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
	    }
	    
	}
	
	    {
	        LOGGER.debug("Debug Message Logged !!!");
	        LOGGER.info("Info Message Logged !!!");
	    }
	    
	    @Test
		public void verifyAmazonPageTitle2() throws InterruptedException {
			
			String getTitle = driver.getTitle();
			Assert.assertEquals(getTitle, "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more");
		    
		    {
		        LOGGER.debug("Debug Message Logged !!!");
		        LOGGER.info("Info Message Logged !!!");
		       // LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
		    }
		    
		}
		
		    {
		        LOGGER.debug("Debug Message Logged !!!");
		        LOGGER.info("Info Message Logged !!!");
		    }
		    
		    @Test
			public void verifyAmazonPageTitle3() throws InterruptedException {
				
				String getTitle = driver.getTitle();
				Assert.assertEquals(getTitle, "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & moree");
			    
			    {
			        LOGGER.debug("Debug Message Logged !!!");
			        LOGGER.info("Info Message Logged !!!");
			       // LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
			    }
			    
			}
			
			    {
			        LOGGER.debug("Debug Message Logged !!!");
			        LOGGER.info("Info Message Logged !!!");
			    }
	
@AfterMethod(alwaysRun=true)
@Parameters({"author", "group"})
public void getResult(ITestResult result, String author, String group) throws IOException
{
   // {
        LOGGER.debug("Debug Message Logged !!!");
        LOGGER.info("Info Message Logged !!!");
       // LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
  //  }
	String methodName = result.getMethod().getMethodName();

	String methodCut = methodName.substring(10);

    if (result.getStatus() == ITestResult.FAILURE)
    {
    	test = extent.createTest(methodName);
    	test.assignCategory("Amazon. " +group);
    	test.assignAuthor(author);
    	test.info("verify amazon "  +methodCut+  " process works correctly");
    	test.info("UserID: AutomationUser1");
        String screenShotPath = TakeShot.capture(driver, "screenShotName");
        test.log(Status.FAIL, MarkupHelper.createLabel(methodName+" test case FAILED due to below issues:", ExtentColor.RED));
        test.fail(result.getThrowable());
        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
    }
   else if(result.getStatus() == ITestResult.SUCCESS)
    {
	   test = extent.createTest(methodName);
	test.assignCategory("Amazon. " +group);
	test.assignAuthor(author);
	test.info("verify amazon "  +methodCut+  " process works correctly");
	test.info("UserID: AutomationUser1");
        test.log(Status.PASS, MarkupHelper.createLabel(methodName+" test case PASSED", ExtentColor.GREEN));
    }
    else
    {
    	test = extent.createTest(methodName);
    	test.assignCategory("Amazon. " +group);
    	test.assignAuthor(author);
    	test.info("verify amazon "  +methodCut+  " process works correctly");
    	test.info("UserID: AutomationUser1");
        test.log(Status.SKIP, MarkupHelper.createLabel(methodName+" test case SKIPPED", ExtentColor.ORANGE));
        test.skip(result.getThrowable());
        
        LOGGER.debug("Debug Message Logged !!!");
        LOGGER.info("Info Message Logged !!!");
        LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));
        
    }
    
    extent.flush();    


    driver.quit();
    
}

}